import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-value",
  templateUrl: "./value.component.html",
  styleUrls: ["./value.component.css"]
})
export class ValueComponent implements OnInit {
  amount: any;
  values: any;
  convertedValue: any;
  rowSelected = false;
  selectedCurrency: any;
  selectedRate; // buy or sell

  options = [{ name: "Buy" }, { name: "Sell" }];

  constructor(private http: HttpClient) {}
  ngOnInit() {
    this.getValues();
  }

  getValues() {
    this.http.get("http://localhost:8080/api/currency/getCurrencies").subscribe(
      data => {
        this.values = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  onChangeofOptions(rate) {
    this.selectedRate = rate;
  }

  RowSelected(currency: any) {
    this.rowSelected = true;
    this.selectedCurrency = currency;
  }

  onChangeRate(rate) {
    console.log("Rate is " + rate);
    this.selectedRate = rate;
  }

  convert() {
    this.http
      .get(
        "http://localhost:8080/api/currency/calculateCurrency/" +
          this.selectedCurrency.name +
          "/" +
          this.selectedRate +
          "/" +
          this.amount
      )
      .subscribe(
        // tslint:disable-next-line: variable-name
        data => {
          this.convertedValue = data;
        },
        error => {
          console.log(error);
        }
      );
  }
}
